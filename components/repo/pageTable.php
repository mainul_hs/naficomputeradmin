<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Blank page
            <small>it all starts here</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Examples</a></li>
            <li class="active">Blank page</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

         <div class="row">
           <div class="col-xs-12">
             <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Table With Full Features</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>USER NAME</th>
                        <th>PASSWORD</th>
                        <th>ACTION</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                      $dataUser = "SELECT * FROM users"; 
                      $user_list  = fetchData($dataUser);
                      foreach ($user_list as $ul) {
                        $id=$ul['id'];
                    ?>
                      <tr>
                        <td><?php echo $ul['id']; ?></td>
                        <td><?php echo $ul['userName']; ?></td> 
                        <td><?php echo $ul['password']; ?></td>
                        <td>
                            <a href="del_user.php?id=<?php echo $id;?>">Delete</a>
                            <a href="edit_user.php?id=<?php echo $id;?>">Edit</a>
                        </td> 
                      </tr>
                    <?php
                      }
                    ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>ID</th>
                        <th>USER NAME</th>
                        <th>PASSWORD</th>
                        <th>ACTION</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
           </div>
         </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->