<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2016 <a href="http://ssoftbd.com">SPREAD SOFTWARE SOLUTION</a>.</strong> All rights reserved.
      </footer>