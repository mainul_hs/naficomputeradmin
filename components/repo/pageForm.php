<?php

  if (isset($_POST['submit'])){
    $userName= $_POST['userName'];
    $password= $_POST['password'];
    
    $userQuery = "INSERT INTO users (userName, password) values('".$userName."','".$password."')"; 
    
    query($userQuery);

    header('location:index.php');
  }

?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      General Form Elements
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title">Horizontal Form</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <form class="form-horizontal" method="POST">
            <div class="form-group">
              <label for="inputUserName" class="col-sm-2 control-label">User Name</label>
              <div class="col-sm-6">
                <input type="text" class="form-control" id="inputUserName" name="userName" placeholder="User Name">
              </div>
            </div>
            <div class="form-group">
              <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
              <div class="col-sm-6">
                <input type="password" class="form-control" id="inputPassword3" name="password" placeholder="Password">
              </div>
            </div>
            <div class="box-body">
              <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-6">
                  <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                </div>
              </div>

              
              <div class="form-group">
                <label for="inputPassword4" class="col-sm-2 control-label">Textarea</label>
                <div class="col-sm-6">
                  <textarea id="inputPassword4" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                </div>
              </div>
              <!-- select -->
              <div class="form-group">
                <label class="col-sm-2 control-label">Select</label>
                <div class="col-sm-6">
                  <select class="form-control">
                    <option>option 1</option>
                    <option>option 2</option>
                    <option>option 3</option>
                    <option>option 4</option>
                    <option>option 5</option>
                  </select>
                </div>
              </div>
              <!-- checkbox -->
              <!-- <div class="form-group">
                <div class="checkbox">
                  <label class="col-sm-2 control-label">
                    <input type="checkbox">
                    Checkbox 1
                  </label>
                </div>

                <div class="checkbox">
                  <label class="col-sm-2 control-label">
                    <input type="checkbox">
                    Checkbox 2
                  </label>
                </div>
              </div> -->
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-6">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox"> Remember me
                    </label>
                  </div>
                </div>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-default" style="margin-left: 15%;">Cancel</button>
              <button name="submit" type="submit" class="btn btn-info pull-right" style="margin-right: 35%;">Sign in</button>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box -->
      </div><!--/.col (right) -->
    </div>   <!-- /.row -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->