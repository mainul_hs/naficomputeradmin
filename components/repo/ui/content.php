<!-- content -->
	    <!-- <section id="content" class="row clearfix"> -->
	    
	        <!-- Middle Col -->
	        <div id="middlecol" class="span8">
	            
	            <div class="inside">
	        		<div id="system-message-container">
	                </div>            
	                <div class="blog-featured">   
	                	<div class="items-leading">

							<div class="leading leading-0">
								<h2 class="item-title">Bangladesh MHM School &amp; College			</h2>
								 <p style="text-align: justify;">Established in 1979, Bangladesh MHM School &amp; College, an English Medium Educational Institution which is run by Bangladesh Embassy Doha, Qatar, has witnessed phenomenal growth and success attributed to its high quality standards and pedagogical leadership. Today, after 35 years of providing academic excellence, the school is internationally renowned as the premier institution.</p>
								 	<ul>
										<li>A well disciplined institution with emphasis on inculcating moral and ethical values.</li>
										<li>Special focus on Kindergarten classes with audio visual aids.</li>
										<li>Follows Secondary and Higher Secondary Curriculum of BISE, Dhaka, Bangladesh.</li>
										<li>Extensive use of multimedia devices in the classrooms</li>
										<li>Different Sections for Boys and Girls</li>
										<li>Transport facilities within reasonable fare</li>
										<li>Compulsory computer education for classes I to XII</li>
										<li>Well equipped Laboratories (Physics, Chemistry, Biology and Computer)</li>
										<li>Spacious library with vast number of reference books</li>
										<li>Special care given for children requiring special needs.</li>
										<li>Wide range of Co-curricular and extra-curricular activities.</li>
										<li>Indoor and Outdoor game facilities.</li>
										<li>Workshop/Seminar on different subjects of current topics.</li>
										<li>A team of highly educated and well trained Faculties.</li>
										<li>24 hours security with close circuit cameras inside and around the school</li>
									</ul>
							</div>	
							<div class="clearfix"></div>
						</div>
						<div class="clearfix"></div>
				    	<div class="items-row cols-2 row-0 row-fluid">
							<div class="item column-1 span6">
								<h2 class="item-title">Facilities			</h2>
								<p style="text-align: justify;">The school has a large well-stocked library with books on a variety of subjects. The Library is open to all students from grade-I onwards. All classes are provided with at least one library period per week. Reference books for diverse disciplines - from pure sciences to forestry, environmental studies to literature and drama, from epics to bestsellers - are available. A number of magazines, daily newspapers will also be available to keep the students abreast of happenings around the world.</p>
								<ul>
									<li>The school has spacious, well-equipped laboratories for Physics, Chemistry and Biology with state-of-the-art equipments to ensure the best in teaching aids. This provides hands on experience and helps in better understanding and retention of the subject.</li>
									<li>The school has the latest in computer software and hardware to provide students with first-hand experience of its varied applications and possibilities. Incorporating the use of modern multi-media technology, computer-aided learning along with computer education will add a new dimension to the curriculum. .</li>
									<li>The School possess a well maintained playground and a cricket pitch with net practice area.</li>
									<li>The school has got 20 well maintained Air Conditioned buses, that moves in and around Doha. Transport facility is available only on request.</li>
								</ul>
							</div>
						</div>
					</div>
	            </div>  
	        </div><!-- / Middle Col  -->
				      
	        <!-- sidebar -->
	        
	        <!-- /sidebar -->  
	    <!-- </section>-->
	    <!-- /content --> 